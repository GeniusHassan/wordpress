
		<div class="ge-blue-footer">
			<footer>
				<div class="container">

                    <?php wp_nav_menu( array( 'theme_location' => 'focal-menu', 'menu_id' => 'primary-footer-menu','menu_class' => 'primary-footer-menu' ) ); ?>

                    <ul>
                        <li><div class="icon"><a href="" target="_blank"><i class="fa fa-twitter"></i></a></div></li>
                        <li><div class="icon"><a href="" target="_blank"><i class="fa fa-facebook"></i></a></div></li>
                        <li><div class="icon"><a href="" target="_blank"><i class="fa fa-instagram"></i></a></div></li>
                        <li><div class="icon"><a href="" target="_blank"><i class="fa fa-pinterest"></i></a></div></li>
                        <li><div class="icon"><a href="" target="_blank"><i class="fa fa-linkedin"></i></a></div></li>
                    </ul>
                    <p>COPY RIGHT 2014 &copy BLUE WITH PASSION</p>
                </div>
			</footer>
		</div>
</body>
</html>
