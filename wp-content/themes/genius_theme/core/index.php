<?php

define('GE_CTC', get_template_directory().'/core'); // => Custom Theme Constant
define('GE_CTC_ASSETS', GE_CTC.'/assets');
define('GE_CTC_INCLUDES', GE_CTC.'/includes');

if ( !class_exists( 'ReduxFramework' ) && GE_CTC_INCLUDES.'/redux-framework/ReduxCore/framework.php' )  {
    require_once( GE_CTC_INCLUDES.'/redux-framework/ReduxCore/framework.php' );
}
if ( !isset( $ge_theme_options ) && file_exists( GE_CTC_INCLUDES.'/redux-framework/ge-theme-config.php' ) ) {
    require_once( GE_CTC_INCLUDES.'/redux-framework/ge-theme-config.php' );
}

/*
*	include other files here
*/

require_once 'global-functions/ge_get_options.php';



/*--------------------------------------------------------------------------*/
/**
*
* Register CSS and JS files.
*
/*--------------------------------------------------------------------------*/

add_action('init','ge_ct_register_script_style');
function ge_ct_register_script_style(){
    /*   CSS   */
    wp_register_style( 'bootstrap-css', get_template_directory_uri()."/core/assets/css/bootstrap.css",array());
    wp_register_style( 'awesome-css', get_template_directory_uri()."/core/assets/css/font-awesome.css",array());
    wp_register_style( 'layout-1-css', get_template_directory_uri()."/core/assets/css/header_blue_1.css",array());
    
    /*   JS  */
    wp_register_script('bootstrap-js',get_template_directory_uri()."/core/assets/js/bootstrap.js", array('jquery'), '1.0', false);
    
}


/**
*
* Enqueue CSS and JS files.
*
*/

add_action('wp_enqueue_scripts','ge_ct_enqueue_script_style');
function ge_ct_enqueue_script_style(){
    /*   CSS   */
    wp_enqueue_style('bootstrap-css');
    wp_enqueue_style('awesome-css');
    wp_enqueue_style('layout-1-css');

    /*   JS  */
    wp_enqueue_script('bootstrap-js');
}