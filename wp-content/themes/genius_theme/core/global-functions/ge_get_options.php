<?php

function ge_get_option($key = '', $default = ''){

    global $ge_theme_options;

    if (isset($ge_theme_options[$key])){
        return !empty($ge_theme_options[$key]) ? $ge_theme_options[$key] : $default;
    }
    return $default;
}