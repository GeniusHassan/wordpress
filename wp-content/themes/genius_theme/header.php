<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package FocalTheme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masthead" class="site-header" role="banner">
<!--		<div class="site-branding">-->

<!--		</div><!-- .site-branding -->
                <div class="logo-bar">
                    <div class="container">
                        <div class="col-md-4 ge-logo">
                            <div class="logo-text"><span>Company</span></div>
                            <p>We Discover Your Talent</p>
                        </div>
                        <div class="col-md-4 ge-search-box">
                            <input type="search" class="form-control" placeholder="Search">
                        </div>
                        <div class="col-md-4">
                            <div class="social">
                                    <ul>
                                        <li><span>Fallow Us</span></li>
                                        <li><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a target="_blank" href="#"><i class="fa fa-youtube"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                    </div>
                </div>
                <nav class="navbar navbar-default ge-nav-menu">
                    <!--    <div class="nav-height">-->
                    <div class="container">
                        <div class="col-md-12">
                            <input type="checkbox" id="navbar-toggle-cbox">
                            <div class="navbar-header">
                                <label for="navbar-toggle-cbox" class="navbar-toggle collapsed" data-toggle="collapse"
                                       data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </label>
                            </div>
                            <div id="navbar" class="navbar-collapse collapse">
                                <?php wp_nav_menu( array( 'theme_location' => 'focal-menu','menu_id' => 'primary-header-menu' ,'menu_class' => 'primary-header-menu' ) ); ?>
                            </div>
                        </div>
                    </div>
                </nav>
	</header><!-- #masthead -->
<!---->
	<div id="content" class="site-content">
